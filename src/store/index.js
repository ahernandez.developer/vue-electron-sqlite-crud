import Vue from "vue";
import Vuex from "vuex";
import { ipcRenderer } from "electron";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    notes: [],
  },
  mutations: {
    updateNotes(state, notes) {
      state.notes = notes;
    },
  },
  actions: {
    fetchNotes({ commit }) {
      ipcRenderer.send("api", {
        to: "notes",
        method: "fetch",
      });
      
      ipcRenderer.on("api/notes/fetch", (event, response) => {
        commit(
          "updateNotes",
          response.map((note) => note.dataValues)
        );
      });
    },
  },
  modules: {},
});
