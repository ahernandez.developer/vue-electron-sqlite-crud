const {Sequelize } = require("sequelize");

import path from 'path'

const isBuild = process.env.NODE_ENV === 'production'

const pathToDbFile = path.join(
  isBuild ? __dirname : __static,
  '../src/database/database.sqlite',
);


function getConection(){
  console.log(pathToDbFile);
    const connection = new Sequelize({
        dialect: "sqlite",
        storage: pathToDbFile,
      });
      
    return connection;
}

export {getConection};