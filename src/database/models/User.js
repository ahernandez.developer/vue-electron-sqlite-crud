const  { DataTypes, Model,Sequelize } =require("sequelize");


const connection = new Sequelize({
  dialect: "sqlite",
  storage: "./database.sqlite",
});


class User extends Model {}

function createUser() {
  User.init(
    {
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastName: {
        type: DataTypes.STRING,
      },
      email: {
        type: DataTypes.STRING,
      },
      password: {
        type: DataTypes.STRING,
      },
    },
    {
      sequelize: connection,
      modelName: "User",
    }
  );

  console.log(User == connection.models.User);
}

export { createUser };
