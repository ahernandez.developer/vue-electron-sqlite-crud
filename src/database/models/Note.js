const { DataTypes, Model } = require("sequelize");
const { getConection } = require("../index");

const connection = getConection();

class Note extends Model {}

function init() {
  Note.init(
    {
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      body: {
        type: DataTypes.STRING,
      },
    },
    {
      sequelize: connection,
      modelName: "Note",
    }
  );
  return Note;
}

export { init };
