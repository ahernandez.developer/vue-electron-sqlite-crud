async function routes(route) {
  console.log("antes de controllador routes.js");
  const control = require("./controllers/" + route.to)[route.method];

  let response = await control(route.params);
  console.log("despues de controllador routes.js");
  return response;
}

export { routes };
