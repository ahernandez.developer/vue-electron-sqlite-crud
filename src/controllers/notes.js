const { init } = require("../database/models/Note");
const { getConection } = require("../database/index");

const noteModel = init();
const connection = getConection();

async function fetch() {
  const response = await noteModel.findAll();
  return response;
}

async function store(params) {
  await connection.sync();
  const note = await noteModel.create({
    title: params.title,
    body: params.content,
  });

  return note;
}

export { fetch, store };
