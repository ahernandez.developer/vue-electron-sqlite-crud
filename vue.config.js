module.exports = {
  
  transpileDependencies: ["vuetify"],
  pluginOptions: {
    electronBuilder: {
      chainWebpackRendererProcess(config) {
        config.plugins.delete('workbox')
        config.plugins.delete('pwa')
      },
      mainProcessWatch: ['src/controllers/*', 'src/database/models/*','src/background.js'],
      nodeIntegration: true,
      externals: [
        "typeorm",
        "sequelize",
        "sequelize-typescript",
        "sqlite3",
        "pg-hstore",
      ],
    },
  },
};
